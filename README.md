# Ejercicio
Este ejercicio demuestra como realizar un proyecto de pruebas unitarias con unittest

## Running the tests

para ejecutar todas las pruebas
```
python -m unittes
```

para ejecutar todas las pruebas y ver el status de cada una
```
python -m unittes -v
```

para ejecutar pruebas de solo ordenar
```
python  -m unittest  test.Test_Ordenar -v
```

para ejecutar pruebas de solo buscar
```
python  -m unittest  test.Test_Buscar -v
```

## Authors

* **Javier Hernandez** - *Xavi Design* - [PurpleBooth](https://bitbucket.org/aceherdz)