class Data:
    def __init__(self,data):
        self.data=data

    def method(self):
        print (f"i'm just a method {self.data}")

class Ordenador(Data):
    def ordenar(self):
        if not self.data:
            return self.data
        sorted = []
        while len(self.data) > 1:
            self.apilar()
            self.intercambiar(0,len(self.data)-1)
            sorted.insert(0,self.data.pop())
        sorted.insert(0,self.data.pop())
        self.data = sorted
        return self.data

    def intercambiar(self,pos1,pos2):
        #print (f"pos1,pos2 {pos1},{pos2}")
        self.data[pos1],self.data[pos2] = self.data[pos2],self.data[pos1]

    def apilar(self):
        pos = len(self.data) // 2
        while pos >= 0:            
            izquierda = 2 * pos + 1
            derecha = 2 * pos + 2 
            self.definirMaximo(pos,izquierda,derecha)
            pos=pos - 1
        
    def definirMaximo(self,raiz,izquierda,derecha):
        if izquierda < len(self.data) and self.data[raiz] < self.data[izquierda]:
            self.intercambiar(raiz,izquierda)
        if derecha < len(self.data)   and self.data[raiz] < self.data[derecha]:
            self.intercambiar(raiz,derecha)

        



class Buscador(Ordenador):
    def buscar(self,valor):
        self.ordenar()
        ini = 0
        fin = len(self.data)
        pos = fin // 2
        while pos > 1:
            pos = ini + (fin - ini) // 2
            if self.data[pos] == valor:
                return pos
            elif valor > self.data[pos]:
                ini = pos 
            else:
                fin = pos
            
        return -1



